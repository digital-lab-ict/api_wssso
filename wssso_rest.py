import os
import json
import logging
import urllib2
import array
from poster.encode import multipart_encode, MultipartParam
from poster.streaminghttp import register_openers

register_openers()

class WSSSORESTClient:
  instances = {}
  API_URL_TEST = "http://st-wl-wsserver.eni.it:8080/up-srvc-aux/services/"
  API_URL_PROD = "http://wl-wsserver.eni.it:8080/up-srvc-aux/services/"

  @classmethod
  def get_instance(cls, url=None):
    if not url:
      url = cls.API_URL_PROD if os.environ.get('API_ENV') == 'PROD' else cls.API_URL_TEST

    instance = cls.instances.get(url)
    if instance is None:
      instance = WSSSORESTClient(url)
      cls.instances[url] = instance
    return instance

  def __init__(self, url):
    self._url  = url

  def get_avatar(self, user_id):
    user_id_type = "cmatrnotes"
    if len(user_id) > 0 and len(user_id) < 10 and user_id[0].isdigit():
      user_id_type = "puser"
    
    req = urllib2.Request(self._url + 'userAvatarImage?format=json&includeimg=true&keytype=' + user_id_type + '&action=download&ok=OK&usertype=pri&key=' + str(user_id))
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      data = json.load(handle)
      image = array.array('b')
      if data.get('uavatarimg'):
        for b in data.get('uavatarimg').split(','):
          image.append(int(b))
        return image.tostring()
      else:
        return None
    except:
  	  raise
    return None

  def set_avatar(self, user_id, avatar_image):
    datagen, headers = multipart_encode([("key", str(user_id)),
                                         ("keytype", "cmatrnotes"),
										 ("usertype", "pri"),
										 ("action", "upload"),
										 ("format", "json"),
										 ("adjust", "scale"),
                                         MultipartParam("image", avatar_image, filetype="image/jpeg", filename="image.jpg")]
                                       )

    req = urllib2.Request(self._url + 'userAvatarImage', datagen, headers)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      resp = json.load(handle)
      return resp
    except IOError, e:
      logging.error(str(e))
      raise

  class UserNotFoundError(Exception):
    def __init__(self, message):
      self.message = message
