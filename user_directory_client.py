import os
import json
import logging
import urllib2
import array

class UserDirectoryClient:
  instances = {}
  API_URL_TEST = "http://02srv008cy.hosts.eni.intranet:8983/solr/user_directory/select"
  API_URL_PROD = "http://api-userdirectory.api-net:8983/solr/user_directory/select"

  @classmethod
  def get_instance(cls, url=None):
    if not url:
      url = cls.API_URL_PROD if os.environ.get('API_ENV') == 'PROD' else cls.API_URL_TEST

    instance = cls.instances.get(url)
    if instance is None:
      instance = cls(url)
      cls.instances[url] = instance
    return instance

  def __init__(self, url):
    self._url  = url

  def get_profile(self, user_id):
    req = urllib2.Request(self._url + '?wt=json&q=C_UID:' + str(user_id).upper())
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      data = json.load(handle)
      if data.get('response').get("numFound") == 1:
        return data.get('response').get("docs")[0]
      else:
        return None
    except:
  	  raise
    return None

  def get_profile_by_user_id_hr(self, user_id_hr):
    req = urllib2.Request(self._url + '?wt=json&q=C_MATR_HR:' + user_id_hr)
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      data = json.load(handle)
      if data.get('response').get("numFound") == 1:
        return data.get('response').get("docs")[0]
      else:
        return None
    except:
  	  raise
    return None

  def get_matrnotes_by_matrhr(self, user_id_hr):
    req = urllib2.Request(self._url + '?wt=json&q=C_MATR_HR:' + str(user_id_hr).upper())
    try:
      handle = urllib2.urlopen(req)
      if handle.getcode() != 200:
        raise IOError("HTTP error: " + str(handle.getcode()))
      data = json.load(handle)
      if data.get('response').get("numFound") == 1:
        return data.get('response').get("docs")[0].get("C_UID")
      else:
        return None
    except:
  	  raise
    return None
