import unittest
import sys
import datetime
import time
import logging
import wssso_rest
import user_directory_client

logging.basicConfig(level=logging.DEBUG)

user = None
image = None

class TestWSSORESTClient(unittest.TestCase):
  def test_get_avatar(self):
    client = wssso_rest.WSSSORESTClient.get_instance(wssso_rest.WSSSORESTClient.url_test)
    image = client.get_avatar(user)
    logging.info("image_len: " + str(len(image)))
    self.assertTrue(image)

  def test_set_avatar(self):
    client = wssso_rest.WSSSORESTClient.get_instance(wssso_rest.WSSSORESTClient.url_test)
    image_data = open(image).read()
    i = client.set_avatar(user, image_data)
    self.assertTrue(i)

  def test_get_profile(self):
    ud_client = user_directory_client.UserDirectoryClient.get_instance()
    u_d_profile = ud_client.get_profile(user)
    self.assertTrue(u_d_profile)

if __name__ == '__main__':
  if len(sys.argv) < 3:
    print "usage: " + sys.argv[0] + " user_id image.jpg"
    exit()
  user = sys.argv[1]
  image = sys.argv[2]
  sys.argv = sys.argv[:1]
  unittest.main()
