import json
import logging
import base64
import wssso
import wssso_rest
import user_directory_client

from flask import Flask, request, Response, abort
from flask.ext.cors import CORS

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
  return "api server v1.0"

@app.route("/wssso/1.0/user/<user_id>", methods=['GET'])
def query_user(user_id):
  logging.info("query_user.1")
  ret_val = {}
  ws_client = wssso.WSSSOClient.get_instance()
  ud_client = user_directory_client.UserDirectoryClient.get_instance()
  try:
    values = ws_client.query_user(user_id)
    u_d_profile = None
    if values.get("cmatrnotes"):
        u_d_profile = ud_client.get_profile(values.get("cmatrnotes"))
    elif values.get("cmatrhr"):
        u_d_profile = ud_client.get_profile_by_user_id_hr(values.get("cmatrhr"))
    values["ctel"] = u_d_profile.get("C_TEL_CMPT")
    ret_val = {"status": "ok", 'values': values }
  except wssso.WSSSOClient.UserNotFoundError:
    ret_val = {"status": "ko", 'error': 'user_not_found' }
    abort(404)

  return json.dumps(ret_val)

@app.route("/wssso/1.0/user/<user_id>", methods=['PUT'])
def update_user(user_id):
  logging.info("update_user.1")
  ret_val = {}
  try:
    ws_client = wssso.WSSSOClient.get_instance()
    input = request.get_data()
    data = json.loads(input)
    values = ws_client.update_user(user_id, data)
    ret_val = {"status": "ok", 'values': values }
  except wssso.WSSSOClient.UserNotFoundError:
    ret_val = {"status": "ko", 'error': 'user_not_found' }
  except ValueError:
    ret_val = {"status": "ko", 'error': 'invalid_values' }
  except:
    ret_val = {"status": "ko", 'error': 'generic' }
  return json.dumps(ret_val)

@app.route("/wssso/1.0/user/<user_id>/avatar", methods=['GET'])
def get_user_avatar(user_id):
  logging.info("query_user.1")
  ret_val = {}
  client = wssso_rest.WSSSORESTClient.get_instance()
  ud_client = user_directory_client.UserDirectoryClient.get_instance()
  try:
    if len(user_id) >= 10 and user_id[0].isdigit():
      user_id = ud_client.get_matrnotes_by_matrhr(user_id)
    image = client.get_avatar(user_id)
    if image:
      fmt = request.args.get("format", "jpg")
      if fmt == "json":
        image_b64 = base64.b64encode(image)
        ret_val = {"status": "ok", 'image_b64': image_b64 }
      else:
        return Response(image, mimetype="image/jpeg")
    else:
      ret_val = {"status": "ko", 'error': 'image_not_available' }
  except wssso_rest.WSSSORESTClient.UserNotFoundError:
    ret_val = {"status": "ko", 'error': 'user_not_found' }

  return json.dumps(ret_val)

@app.route("/wssso/1.0/user/<user_id>/avatar", methods=['PUT'])
def set_user_avatar(user_id):
  logging.info("avatar_user.1")
  ret_val = {}
  client = wssso_rest.WSSSORESTClient.get_instance()
  try:
    input = request.get_data()
    data = json.loads(input)
    image_data = base64.b64decode(data.get('image_b64'))
    data = client.set_avatar(user_id, image_data)
    ret_val = {"status": "ok", 'values': data }
  except wssso_rest.WSSSORESTClient.UserNotFoundError:
    ret_val = {"status": "ko", 'error': 'user_not_found' }
  except:
    logging.error("input data:" + str(request.get_data()) )
  return json.dumps(ret_val)

@app.route("/test")
def test():
  return app.send_static_file('test.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0',port=5000,threaded=True)
