import os
import io
import json
import logging
from suds.client import Client

class WSSSOClient:
  API_URL_TEST = "file:/appl/api_wssso/wssso.test.wsdl"
  API_URL_PROD = "file:/appl/api_wssso/wssso.prod.wsdl"
  instances = {}

  @classmethod
  def get_instance(cls, url=None):
    if not url:
      url = cls.API_URL_PROD if os.environ.get('API_ENV') == 'PROD' else cls.API_URL_TEST
    instance = cls.instances.get(url)
    if instance is None:
      instance = WSSSOClient(url)
      cls.instances[url] = instance
    return instance

  def __init__(self, url):
    self._client  = Client(url)

  def query_user(self, user_id):
    query_user_input = self._client.factory.create('ns0:WssoSrvcUserQueryUserIn')

    if len(user_id) > 0 and len(user_id) < 10 and user_id[0].isdigit():
      query_user_input.puser = user_id
    elif len(user_id) >= 10 and user_id[0].isdigit():
      query_user_input.cmatrhr = user_id
    else:
      query_user_input.cmatrnotes = user_id

    response = self._client.service.queryUser(query_user_input)
    values = {}
    for i in response:
      values[i[0]] = i[1]

    if values.get("puser", 0) == 0:
        raise self.UserNotFoundError("user_not_found")

    return values

  def update_user(self, user_id, values):
    user = self.query_user(user_id)
    if user.get('puser') == 0:
      raise self.UserNotFoundError(user.message)

    user_update_input = self._client.factory.create('ns0:WssoSrvcUserUpdateUserIn')
    user_update_input.puser = user.get('puser')
    user_update_input.administrator = 60

    for key, value in values.items():
      if hasattr(user_update_input, key):
        setattr(user_update_input, key, value)
    response = self._client.service.updateUser(user_update_input)
    values = {}
    for i in response:
      values[i[0]] = i[1]
    return values

  class UserNotFoundError(Exception):
    def __init__(self, message):
      self.message = message
